package main

import (
	"github.com/sedmess/go-ctx/ctx"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

const controllerServiceName = "controller_service"

const linkPatternOption = "LINK_PATTERN"
const linkPatternDefault = "https://cloud.mail.ru/public/(.*)"
const fileNamePatternOption = "FILE_NAME_PATTERN"
const fileNamePatternDefault = "(.*)\\s\\/\\sОблако Mail\\.ru"
const targetDirectoryOption = "TARGET_DIRECTORY"
const personalTargetDirectoriesOption = "PERSONAL_TARGET_DIRECTORIES"
const unzipOnFlyOption = "UNZIP_ON_FLY"

const MsgInit = 0
const MsgInter = 1
const MsgFinal = 2
const MsgSingle = 3

const Default = 0
const DownloadOnly = 1
const DownloadUnzip = 2
const DownloadUnzipOnFly = 3
const GetActiveTasks = 4

//const CancelTask = 5

type CreateTaskRequest struct {
	chatId int64
	url    string
	option int
}
type TaskStatus struct {
	taskId  int
	chatId  int64
	url     string
	name    string
	state   string
	msgType int
}

type Message struct {
	chatId int64
	msg    string
}

type taskSet struct {
	sync.Mutex

	tasks map[int64]*map[string]int64
}

func (instance *taskSet) tryAddTask(chatId int64, url string) bool {
	instance.Lock()
	defer instance.Unlock()

	chatTaskMap, found := instance.tasks[chatId]
	if !found {
		m := make(map[string]int64)
		chatTaskMap = &m
	}
	_, found = (*chatTaskMap)[url]
	if found {
		return false
	}
	(*chatTaskMap)[url] = time.Now().UnixMilli()
	instance.tasks[chatId] = chatTaskMap
	return true
}

func (instance *taskSet) markTaskDone(chatId int64, url string) {
	instance.Lock()
	defer instance.Unlock()

	chatTaskMap := instance.tasks[chatId]

	delete(*chatTaskMap, url)

	if len(*chatTaskMap) == 0 {
		delete(instance.tasks, chatId)
	}
}

func (instance *taskSet) getTasks(chatId int64) *map[string]int64 {
	instance.Lock()
	defer instance.Unlock()

	chatTaskMap, found := instance.tasks[chatId]
	if found {
		return chatTaskMap
	} else {
		return nil
	}
}

type ControllerService struct {
	ctx.BasicConnector

	restClientService *RestClientService

	tasks        *taskSet
	taskCounter  int32
	taskCancelCh chan string

	linkPattern        *regexp.Regexp
	fileNamePattern    *regexp.Regexp
	targetDirectory    string
	personalTargetDirs *map[int64]string
	unzipOnFly         bool
}

func createControllerService() *ControllerService {
	service := &ControllerService{}
	service.BasicConnector = ctx.NewBasicConnector(service)
	return service
}

func (instance *ControllerService) Init(serviceProvider func(serviceName string) ctx.Service) {
	instance.restClientService = serviceProvider(restClientServiceName).(*RestClientService)

	instance.tasks = &taskSet{
		tasks: make(map[int64]*map[string]int64),
	}

	instance.linkPattern = regexp.MustCompile(
		ctx.GetEnv(linkPatternOption).AsStringDefault(linkPatternDefault),
	)

	instance.fileNamePattern = regexp.MustCompile(
		ctx.GetEnv(fileNamePatternOption).AsStringDefault(fileNamePatternDefault),
	)

	instance.targetDirectory = ctx.GetEnv(targetDirectoryOption).AsString()

	personalTargetDirs := make(map[int64]string)
	instance.personalTargetDirs = &personalTargetDirs
	personalTargetDirValue := ctx.GetEnv(personalTargetDirectoriesOption)
	if personalTargetDirValue.IsPresent() {
		kvMap := personalTargetDirValue.AsMap()

		for key, value := range *kvMap {
			chatId, err := strconv.ParseInt(key, 10, 64)
			if err != nil {
				panic(err)
			}
			personalTargetDirs[chatId] = value
		}
	}

	instance.unzipOnFly = ctx.GetEnv(unzipOnFlyOption).AsBoolDefault(false)
}

func (instance *ControllerService) Name() string {
	return controllerServiceName
}

func (instance *ControllerService) Dispose() {
}

func (instance *ControllerService) OnMessage(msg interface{}) {
	request := msg.(CreateTaskRequest)
	ctx.LogInfo(controllerServiceName, strconv.FormatInt(request.chatId, 10)+": "+request.url)

	if request.option == GetActiveTasks {
		go instance.taskList(request.chatId)
	} else {
		go instance.downloadAndUnpack(request.chatId, request.url, request.option)
	}
}

func (instance *ControllerService) defineTargetDirectory(chatId int64) string {
	if dir, found := (*instance.personalTargetDirs)[chatId]; found {
		return dir
	} else {
		return instance.targetDirectory
	}
}

func (instance *ControllerService) downloadAndUnpack(chatId int64, url string, option int) {
	ctx.LogInfo(controllerServiceName, "chatId "+strconv.FormatInt(chatId, 10)+" url "+url+": start")
	defer ctx.LogInfo(controllerServiceName, "chatId "+strconv.FormatInt(chatId, 10)+" url "+url+": finish")

	ctx.RunWithRecover(
		func() {
			if !instance.tasks.tryAddTask(chatId, url) {
				instance.Send(TaskStatus{chatId: chatId, msgType: MsgFinal, state: "❌ Error: this url has already requested", url: url})
				return
			}
			defer instance.tasks.markTaskDone(chatId, url)

			taskId := int(atomic.AddInt32(&instance.taskCounter, 1))

			fileSize := int64(-1)
			urlSubmatch := instance.linkPattern.FindStringSubmatch(url)
			var uri string
			name := randString(16)
			if len(urlSubmatch) > 1 {
				uri = urlSubmatch[1]
			} else {
				instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "❌ Error: can't extract uri of the file", url: url})
				return
			}
			instance.Send(TaskStatus{taskId: taskId, msgType: MsgInit, chatId: chatId, state: "⏳ So, uri will be: " + uri, url: url})

			instance.Send(TaskStatus{taskId: taskId, msgType: MsgInter, chatId: chatId, state: "⏳ Let's try to get the real file name", url: url})
			folderInfo, err := instance.restClientService.GetDownloadInfo(uri)
			if err != nil {
				instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "❌ Error: can't the real file name", url: url})
				return
			} else {
				name = folderInfo.Name
				fileSize = folderInfo.Size
			}

			instance.Send(TaskStatus{taskId: taskId, msgType: MsgInter, chatId: chatId, state: "⏳ Resolving the download link", name: name, url: url})

			downloadFileName := randString(16)
			link, err := instance.restClientService.GetDownloadLink("anonym", uri, downloadFileName)
			if err != nil {
				instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "❌ Error: can't resolve the download link: " + err.Error(), name: name, url: url})
				return
			}
			instance.Send(TaskStatus{taskId: taskId, msgType: MsgInter, chatId: chatId, state: "⏳ We're about to start downloading, be patient. You'll get some info messages. Expected size: " + byteCountIEC(fileSize), name: name, url: url})

			path := instance.defineTargetDirectory(chatId)

			if instance.unzipOnFly && option == Default || option == DownloadUnzipOnFly {
				ctx.LogInfo(controllerServiceName, "downloading "+url+" with unziping on the fly")
				startTS := time.Now()
				size, err := instance.restClientService.DownloadZipFile(link, path, func(currentFilename string, savedSize int64) {
					interDownloadTime := time.Now().Unix() - startTS.Unix()
					if interDownloadTime < 1 {
						interDownloadTime = 1
					}
					interDownloadRate := savedSize / interDownloadTime
					msg := "Downloading " + currentFilename + "; saved " + byteCountIEC(savedSize) + " so far ( ~ " + byteCountIEC(interDownloadRate) + "/s) of " + byteCountIEC(fileSize)
					ctx.LogDebug(controllerServiceName, msg)
					instance.Send(TaskStatus{taskId: taskId, chatId: chatId, state: "⏳ " + msg, name: name, url: url})
				})
				endTS := time.Now()
				downloadTime := endTS.Unix() - startTS.Unix()
				if downloadTime < 1 {
					downloadTime = 1
				}
				if err != nil {
					instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "❌ Error: can't download file:" + err.Error() + "; downloaded " + byteCountIEC(size) + " of " + byteCountIEC(size), name: name, url: url})
					return //todo remove file?
				}
				downloadRate := size / downloadTime
				instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "✅ Hooray! The file has been downloaded (fileSize: " + byteCountIEC(size) + "; ~ " + byteCountIEC(downloadRate) + "/s)", name: name, url: url})
				//todo deep unzip
			} else {
				ctx.LogInfo(controllerServiceName, "downloading "+url+" then unziping")
				tmpPath := filepath.Join(path, downloadFileName+".zip")
				startTS := time.Now()
				size, err := instance.restClientService.DownloadFile(link, tmpPath, func(savedSize int64) {
					interDownloadTime := time.Now().Unix() - startTS.Unix()
					if interDownloadTime < 1 {
						interDownloadTime = 1
					}
					interDownloadRate := savedSize / interDownloadTime
					msg := "Downloading archive; saved " + byteCountIEC(savedSize) + " so far ( ~ " + byteCountIEC(interDownloadRate) + "/s) of " + byteCountIEC(fileSize)
					ctx.LogDebug(controllerServiceName, msg)
					instance.Send(TaskStatus{taskId: taskId, chatId: chatId, state: "⏳ " + msg, name: name, url: url})
				})
				endTS := time.Now()
				downloadTime := endTS.Unix() - startTS.Unix()
				if downloadTime < 1 {
					downloadTime = 1
				}
				if err != nil {
					instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "❌ Error: can't download archive:" + err.Error() + "; downloaded " + byteCountIEC(size) + " of " + byteCountIEC(fileSize), name: name, url: url})
					return //todo remove file?
				}
				downloadRate := size / downloadTime

				if option == DownloadOnly {
					newPath := filepath.Join(path, name+".zip")
					err := os.Rename(tmpPath, newPath)
					if err != nil {
						ctx.LogError(controllerServiceName, "can't rename "+tmpPath+" to "+newPath+": "+err.Error())
					}
					instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "✅ Hooray! The archive has been downloaded (fileSize: " + byteCountIEC(size) + "; ~ " + byteCountIEC(downloadRate) + "/s)", name: name, url: url})
					return
				}
				instance.Send(TaskStatus{taskId: taskId, msgType: MsgInter, chatId: chatId, state: "⏳ Hooray! The archive has been downloaded (fileSize: " + byteCountIEC(size) + "; ~ " + byteCountIEC(downloadRate) + "/s)", name: name, url: url})

				startTS = time.Now()
				size, err = unzip(tmpPath, path, func(currentFilename string, savedSize int64) {
					interUnzipTime := time.Now().Unix() - startTS.Unix()
					if interUnzipTime < 1 {
						interUnzipTime = 1
					}
					interDownloadRate := savedSize / interUnzipTime
					msg := "Unziping " + currentFilename + "; saved " + byteCountIEC(savedSize) + " so far ( ~ " + byteCountIEC(interDownloadRate) + "/s) of " + byteCountIEC(fileSize)
					ctx.LogDebug(controllerServiceName, msg)
					instance.Send(TaskStatus{taskId: taskId, chatId: chatId, state: "⏳ " + msg, name: name, url: url})
				})
				endTS = time.Now()
				unzipTime := endTS.Unix() - startTS.Unix()
				if unzipTime < 1 {
					unzipTime = 1
				}
				if err != nil {
					instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "❌ Error: can't download file:" + err.Error() + "; downloaded " + byteCountIEC(size) + " of " + byteCountIEC(fileSize), name: name, url: url})
					return //todo remove file?
				}

				err = os.Remove(tmpPath)
				if err != nil {
					ctx.LogError(controllerServiceName, "can't remove tmp file "+tmpPath+": "+err.Error())
				}

				unzipRate := size / unzipTime
				instance.Send(TaskStatus{taskId: taskId, msgType: MsgFinal, chatId: chatId, state: "✅ Hooray! The file has been unzipped (fileSize: " + byteCountIEC(size) + "; ~ " + byteCountIEC(unzipRate) + "/s)", name: name, url: url})
				//todo deep unzip
			}
		},
		func(err error) {
			ctx.LogError(controllerServiceName, "⚠", chatId, ":", url, ":", err)
			instance.Send(TaskStatus{msgType: MsgSingle, chatId: chatId, url: url, state: "Something has gone wrong, downloading was interrupted\n" + err.Error()})
		},
	)
}

func (instance *ControllerService) taskList(chatId int64) {
	tasks := instance.tasks.getTasks(chatId)
	if tasks == nil {
		instance.Send(Message{chatId: chatId, msg: "No active tasks"})
	} else {
		msg := ""
		for url, timestamp := range *tasks {
			msg = msg + url + " since " + time.UnixMilli(timestamp).Format("02.01.06 15:04:05") + "\n"
		}
		instance.Send(Message{chatId: chatId, msg: msg})
	}
}

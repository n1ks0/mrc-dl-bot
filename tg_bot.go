package main

import (
	"github.com/sedmess/go-ctx/ctx"
	tb "gopkg.in/telebot.v3"
	"mrc-dl-bot/buildinfo"
	"strconv"
	"strings"
	"sync"
	"time"
)

const tgTokenOption = "BOT_TOKEN"
const tgAllowedChatsOption = "CHAT_IDS"
const tgBotServiceName = "tg_bot_service"

type taskState struct {
	sync.Mutex
	msgId *int
}

type taskStateStore struct {
	sync.Mutex
	m map[int]*taskState
}

func (store *taskStateStore) getOrCreate(taskId int) *taskState {
	defer store.Unlock()
	store.Lock()

	state, found := store.m[taskId]
	if found {
		return state
	} else {
		state = &taskState{}
		store.m[taskId] = state
		return state
	}
}

func (store *taskStateStore) delete(taskId int) {
	defer store.Unlock()
	store.Lock()

	delete(store.m, taskId)
}

type tgBotService struct {
	ctx.BasicConnector

	sync.WaitGroup
	bot *tb.Bot

	allowedChatIds map[int64]interface{}

	taskStateStore *taskStateStore
}

func createTgBotService() *tgBotService {
	service := &tgBotService{}
	service.BasicConnector = ctx.NewBasicConnector(service)
	return service
}

func (instance *tgBotService) Init(_ func(serviceName string) ctx.Service) {
	bot, err := tb.NewBot(tb.Settings{
		Token:  ctx.GetEnv(tgTokenOption).AsString(),
		Poller: &tb.LongPoller{Timeout: 5 * time.Second},
	})
	if err != nil {
		panic("can't create tgbot: " + err.Error())
	}

	instance.allowedChatIds = make(map[int64]interface{})
	allowedChatIds := ctx.GetEnv(tgAllowedChatsOption).AsStringArray()
	for _, chatId := range allowedChatIds {
		chatIdNumber, err := strconv.ParseInt(chatId, 10, 64)
		if err != nil {
			panic(err)
		}
		instance.allowedChatIds[chatIdNumber] = true
	}

	instance.taskStateStore = &taskStateStore{m: make(map[int]*taskState)}

	instance.bot = bot
	instance.bot.Handle(tb.OnText, func(context tb.Context) error {
		switch context.Text() {
		case "/start":
			return nil
		case "/appinfo":
			_, err := instance.bot.Send(&tb.Chat{ID: context.Chat().ID}, "Version "+buildinfo.Version+" of "+buildinfo.BuildDate)
			if err != nil {
				ctx.LogError(tgBotServiceName, "can't send message to "+strconv.FormatInt(context.Chat().ID, 10), err)
			}
			return nil
		case "/chatid":
			_, err := bot.Send(&tb.Chat{ID: context.Chat().ID}, strconv.FormatInt(context.Chat().ID, 10))
			if err != nil {
				return err
			}
			return nil
		default:
			if _, found := instance.allowedChatIds[context.Chat().ID]; !found {
				ctx.LogInfo(tgBotServiceName, "unknown chat id "+strconv.FormatInt(context.Chat().ID, 10))
				return nil
			}

			if strings.HasPrefix(context.Message().Text, "/jd ") { //just download
				instance.Send(CreateTaskRequest{
					chatId: context.Chat().ID,
					url:    strings.TrimLeft(context.Message().Text, "/jd "),
					option: DownloadOnly,
				})
			} else if len(context.Message().Text) > 4 && strings.HasPrefix(context.Message().Text, "/du ") { //download and unzip
				instance.Send(CreateTaskRequest{
					chatId: context.Chat().ID,
					url:    strings.TrimLeft(context.Message().Text, "/du "),
					option: DownloadUnzip,
				})
			} else if len(context.Message().Text) > 5 && strings.HasPrefix(context.Message().Text, "/duf ") { //download and unzip on the fly
				instance.Send(CreateTaskRequest{
					chatId: context.Chat().ID,
					url:    strings.TrimLeft(context.Message().Text, "/duf "),
					option: DownloadUnzipOnFly,
				})
			} else if context.Message().Text == "/tasks" {
				instance.Send(
					CreateTaskRequest{
						chatId: context.Chat().ID,
						option: GetActiveTasks,
					})
			} else {
				instance.Send(CreateTaskRequest{
					chatId: context.Chat().ID,
					url:    context.Message().Text,
					option: Default,
				})
			}
		}
		return nil
	})
}

func (instance *tgBotService) Name() string {
	return tgBotServiceName
}

func (instance *tgBotService) Dispose() {
}

func (instance *tgBotService) AfterStart() {
	go func() {
		instance.Add(1)
		defer instance.Done()
		ctx.LogInfo(tgBotServiceName, "tgbot start")
		instance.bot.Start()
	}()

	instance.BasicConnector.AfterStart()
}

func (instance *tgBotService) BeforeStop() {
	instance.bot.Stop()
	instance.Wait()
	ctx.LogInfo(tgBotServiceName, "tgbot stopped")

	instance.BasicConnector.BeforeStop()
}

func (instance *tgBotService) OnMessage(msg interface{}) {
	status, ok := msg.(TaskStatus)
	if ok {
		instance.onStatus(status)
	} else {
		message, ok := msg.(Message)
		if ok {
			instance.onTestMessage(message)
		}
	}
}

func (instance *tgBotService) onStatus(status TaskStatus) {
	msgString := status.url + "\n" + status.state
	if status.name != "" {
		msgString = msgString + "\nName: " + status.name
	}
	msgString = msgString + "\n\n" + time.Now().Format("02.01.06 15:04:05.000")

	if status.msgType == MsgSingle {
		_, err := instance.bot.Send(&tb.Chat{ID: status.chatId}, msgString)
		if err != nil {
			ctx.LogError(tgBotServiceName, "can't send message to "+strconv.FormatInt(status.chatId, 10), err)
		}
		return
	}

	taskState := instance.taskStateStore.getOrCreate(status.taskId)

	func() {
		defer taskState.Unlock()
		taskState.Lock()

		msgId := taskState.msgId
		if msgId != nil {
			editedMsg, err := instance.bot.Edit(&tb.Message{Chat: &tb.Chat{ID: status.chatId}, ID: *msgId}, msgString)
			if err != nil {
				ctx.LogError(tgBotServiceName, "can't edit message "+strconv.Itoa(*msgId)+" in chat "+strconv.FormatInt(status.chatId, 10), err)
			} else {
				taskState.msgId = &editedMsg.ID
			}
		} else {
			sentMsg, err := instance.bot.Send(&tb.Chat{ID: status.chatId}, msgString)
			if err != nil {
				ctx.LogError(tgBotServiceName, "can't send message to "+strconv.FormatInt(status.chatId, 10), err)
			} else {
				taskState.msgId = &sentMsg.ID
			}
		}
	}()

	if status.msgType == MsgFinal {
		instance.taskStateStore.delete(status.taskId)
	}
}

func (instance *tgBotService) onTestMessage(msg Message) {
	_, err := instance.bot.Send(&tb.Chat{ID: msg.chatId}, msg.msg)
	if err != nil {
		ctx.LogError(tgBotServiceName, "can't send message to "+strconv.FormatInt(msg.chatId, 10), err)
	}
}

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/sedmess/go-ctx/ctx"
	"io"
	"mrc-dl-bot/zipstream"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
)

const restClientServiceName = "rest_client_service"

const folderInfoApiEndpoint = "https://cloud.mail.ru/api/v2/folder?weblink="
const weblinkApiEndpoint = "https://cloud.mail.ru/api/v3/zip/weblink"
const contentTypeApplicationJson = "application/json"
const downloadBatchSize = 52428800 // 50 MB

type RestClientService struct {
}

func (instance *RestClientService) Init(_ func(serviceName string) ctx.Service) {

}

func (instance *RestClientService) Name() string {
	return restClientServiceName
}

func (instance *RestClientService) Dispose() {
}

func (instance *RestClientService) GetDownloadInfo(resource string) (*FolderInfo, error) {
	response, err := http.Get(folderInfoApiEndpoint + url.QueryEscape(resource))
	if err != nil {
		ctx.LogError(restClientServiceName, "on resolving folder info "+resource+": "+err.Error())
		return nil, err
	}
	responseBytes, _ := io.ReadAll(response.Body)
	var folderInfoResp FolderInfoResponse
	err = json.Unmarshal(responseBytes, &folderInfoResp)
	if err != nil {
		ctx.LogError(restClientServiceName, "on resolving folder info "+resource+": "+err.Error())
		return nil, err
	}
	return &folderInfoResp.Body, nil
}

func (instance *RestClientService) GetDownloadLink(email string, resource string, name string) (string, error) {
	weblinkRequest := WeblinkRequest{
		Email:       email,
		WeblinkList: []string{resource},
		Name:        name,
	}
	requestBytes, _ := json.Marshal(&weblinkRequest)
	response, err := http.Post(weblinkApiEndpoint, contentTypeApplicationJson, bytes.NewReader(requestBytes))
	if err != nil {
		ctx.LogError(restClientServiceName, "on resolving "+resource+": "+err.Error())
		return "", err
	}
	defer closeSilently(response.Body)
	responseBytes, _ := io.ReadAll(response.Body)
	var weblinkResponse WeblinkResponse
	err = json.Unmarshal(responseBytes, &weblinkResponse)
	if err != nil {
		ctx.LogError(restClientServiceName, "on resolving "+resource+": "+err.Error())
		return "", err
	}
	return weblinkResponse.Key, nil
}

func (instance *RestClientService) DownloadFile(url string, path string, progressListener func(savedSize int64)) (int64, error) {
	response, err := http.Get(url)
	if err != nil {
		return 0, err
	}
	defer closeSilently(response.Body)

	totalSavedSize := int64(0)

	file, err := os.Create(path)
	if err != nil {
		return totalSavedSize, errors.New("can't create file " + path + ": " + err.Error())
	}

	defer closeSilently(file)
	for {
		written, err := io.CopyN(file, response.Body, downloadBatchSize)
		totalSavedSize += written
		if err != nil {
			if err == io.EOF {
				break
			}
			return totalSavedSize, errors.New("can't write file " + path + ": " + err.Error())
		}
		progressListener(totalSavedSize)
	}

	return totalSavedSize, nil
}

func (instance *RestClientService) DownloadZipFile(url string, path string, progressListener func(currentFilename string, savedSize int64)) (int64, error) {
	response, err := http.Get(url)
	if err != nil {
		return 0, errors.New("can't download " + url + ": " + err.Error())
	}
	defer closeSilently(response.Body)

	totalSavedSize := int64(0)

	zipStream := zipstream.NewReader(response.Body)
	for {
		header, err := zipStream.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			if err.Error() == "Zip64 not yet supported" {
				ctx.LogError(restClientServiceName, "zip64 error on zipstream.Next(), try to continue. url: "+url)
				continue
			}
			ctx.LogError(restClientServiceName, "on downloading "+url+": "+err.Error())
			return totalSavedSize, err
		}
		if header.FileInfo().IsDir() {
			if err := os.MkdirAll(filepath.Join(path, header.Name), 0770); err != nil {
				return 0, err
			}
		} else {
			filePath := filepath.Join(path, header.Name)
			progressListener(header.Name, totalSavedSize)
			if err = makePathDir(filePath); err != nil {
				return 0, err
			}
			file, err := os.Create(filePath)
			if err != nil {
				ctx.LogError(restClientServiceName, "on downloading "+url+": "+err.Error())
				return totalSavedSize, errors.New("can't create file " + header.Name + ": " + err.Error())
			}

			written, err := io.Copy(file, zipStream)

			closeSilently(file)

			if err != nil {
				ctx.LogError(restClientServiceName, "on downloading "+url+": "+err.Error())
				return 0, errors.New("can't write file " + header.Name + ": " + err.Error())
			}

			totalSavedSize += written
		}
	}

	return totalSavedSize, nil
}

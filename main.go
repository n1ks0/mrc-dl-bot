package main

import (
	"github.com/sedmess/go-ctx/ctx"
	"gopkg.in/natefinch/lumberjack.v2"
	"log"
)

const logFilePath = "LOG_FILE_PATH"

func main() {
	logFilePathVar := ctx.GetEnv(logFilePath)
	if logFilePathVar.IsPresent() {
		loggerFile := lumberjack.Logger{
			Filename:   logFilePathVar.AsString(),
			MaxSize:    10,
			MaxBackups: 3,
			MaxAge:     30,
			Compress:   true,
		}
		log.SetOutput(ctx.NewSystemOutProxyWriter(&loggerFile))
	}

	ctx.StartContextualizedApplication(
		ctx.ServiceArray(
			createTgBotService(),
			createControllerService(),
			&RestClientService{},
			ctx.ConnectServices(tgBotServiceName, controllerServiceName),
		),
	)
}

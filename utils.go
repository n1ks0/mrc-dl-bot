package main

import (
	"fmt"
	"github.com/sedmess/go-ctx/ctx"
	"io"
	"math/rand"
	"os"
	"path/filepath"
)

func closeSilently(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		ctx.LogError("CLOSE_RESOURCE_ERROR", err)
	}
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func makePathDir(path string) error {
	dirPath := filepath.Dir(path)
	if err := os.MkdirAll(dirPath, 0770); err != nil {
		return err
	}
	return nil
}

func byteCountIEC(b int64) string {
	if b <= 0 {
		return "unknown"
	}
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %ciB",
		float64(b)/float64(div), "KMGTPE"[exp])
}

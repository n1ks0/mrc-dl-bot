package main

import (
	"archive/zip"
	"errors"
	"io"
	"os"
	"path/filepath"
)

func unzip(file string, target string, progressListener func(currentFilename string, savedSize int64)) (int64, error) {
	reader, err := zip.OpenReader(file)
	if err != nil {
		return 0, errors.New("can't open archive " + file + ": " + err.Error())
	}
	defer closeSilently(reader)

	writtenTotal := int64(0)

	for _, file := range reader.File {
		filePath := filepath.Join(target, file.Name)
		if file.FileInfo().IsDir() {
			if err := os.MkdirAll(filePath, 0770); err != nil {
				return writtenTotal, errors.New("can't create directory" + filePath + ": " + err.Error())
			}
		} else {
			progressListener(file.Name, writtenTotal)

			filePath := filePath

			targetFile, err := os.Create(filePath)
			if err != nil {
				return writtenTotal, errors.New("can't create " + targetFile.Name() + ": " + err.Error())
			}

			reader, err := file.Open()

			if err != nil {
				return writtenTotal, err
			}
			written, err := io.Copy(targetFile, reader)
			writtenTotal += written

			closeSilently(targetFile)

			if err != nil {
				return writtenTotal, errors.New("can't unzip to " + targetFile.Name() + ": " + err.Error())
			}
		}
	}
	return writtenTotal, nil
}

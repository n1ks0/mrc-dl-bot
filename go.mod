module mrc-dl-bot

go 1.19

require (
	github.com/sedmess/go-ctx v0.5.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/telebot.v3 v3.0.0
)

require (
	github.com/BurntSushi/toml v1.2.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

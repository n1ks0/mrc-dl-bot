package main

type FolderInfoResponse struct {
	Body   FolderInfo `json:"body"`
	Time   int64      `json:"time"`
	Status int        `json:"status"`
}

type FolderInfo struct {
	Name string `json:"name"`
	Size int64  `json:"size"`
}

type WeblinkRequest struct {
	Email       string   `json:"x-email"`
	WeblinkList []string `json:"weblink_list"`
	Name        string   `json:"name"`
}

type WeblinkResponse struct {
	Key string `json:"key"`
}
